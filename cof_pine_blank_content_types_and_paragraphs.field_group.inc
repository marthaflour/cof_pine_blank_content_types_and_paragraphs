<?php
/**
 * @file
 * cof_pine_blank_content_types_and_paragraphs.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function cof_pine_blank_content_types_and_paragraphs_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_additional_info|node|article|teaser';
  $field_group->group_name = 'group_additional_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'article';
  $field_group->mode = 'teaser';
  $field_group->parent_name = 'group_row2';
  $field_group->data = array(
    'label' => 'Additional Info',
    'weight' => '9',
    'children' => array(
      0 => 'field_additional_information',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Additional Info',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'col-sm-12 group-additional-info field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_additional_info|node|article|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_article_image|node|article|articles_list';
  $field_group->group_name = 'group_article_image';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'article';
  $field_group->mode = 'articles_list';
  $field_group->parent_name = 'group_row1';
  $field_group->data = array(
    'label' => 'Article List Image',
    'weight' => '-1',
    'children' => array(
      0 => 'field_image',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Article List Image',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'col-sm-4 group-article-image field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_article_image|node|article|articles_list'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_article_summary|node|article|articles_list';
  $field_group->group_name = 'group_article_summary';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'article';
  $field_group->mode = 'articles_list';
  $field_group->parent_name = 'group_row1';
  $field_group->data = array(
    'label' => 'Summary',
    'weight' => '1',
    'children' => array(
      0 => 'body',
      1 => 'field_additional_information',
      2 => 'field_tagline',
      3 => 'title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Summary',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'col-sm-8 group-article-summary field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_article_summary|node|article|articles_list'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_row1|node|article|articles_list';
  $field_group->group_name = 'group_row1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'article';
  $field_group->mode = 'articles_list';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Row 1',
    'weight' => '0',
    'children' => array(
      0 => 'group_article_image',
      1 => 'group_article_summary',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Row 1',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'row group-row1 field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_row1|node|article|articles_list'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_row1|node|article|teaser';
  $field_group->group_name = 'group_row1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'article';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Row 1',
    'weight' => '-1',
    'children' => array(
      0 => 'group_tagline_and_summary',
      1 => 'group_teaser_image',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Row 1',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'row group-row1 field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_row1|node|article|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_row2|node|article|teaser';
  $field_group->group_name = 'group_row2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'article';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Row 2',
    'weight' => '0',
    'children' => array(
      0 => 'group_additional_info',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Row 2',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'row group-row2 field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_row2|node|article|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_tagline_and_summary|node|article|teaser';
  $field_group->group_name = 'group_tagline_and_summary';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'article';
  $field_group->mode = 'teaser';
  $field_group->parent_name = 'group_row1';
  $field_group->data = array(
    'label' => 'Abstract',
    'weight' => '1',
    'children' => array(
      0 => 'body',
      1 => 'field_tagline',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Abstract',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'col-sm-6 group-tagline-and-summary field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_tagline_and_summary|node|article|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_teaser_image|node|article|teaser';
  $field_group->group_name = 'group_teaser_image';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'article';
  $field_group->mode = 'teaser';
  $field_group->parent_name = 'group_row1';
  $field_group->data = array(
    'label' => 'Teaser Image',
    'weight' => '0',
    'children' => array(
      0 => 'field_image',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Teaser Image',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'col-sm-3 group-teaser-image field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_teaser_image|node|article|teaser'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Abstract');
  t('Additional Info');
  t('Article List Image');
  t('Row 1');
  t('Row 2');
  t('Summary');
  t('Teaser Image');

  return $field_groups;
}
