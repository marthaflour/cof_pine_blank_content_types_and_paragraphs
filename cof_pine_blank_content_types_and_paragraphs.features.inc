<?php
/**
 * @file
 * cof_pine_blank_content_types_and_paragraphs.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cof_pine_blank_content_types_and_paragraphs_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function cof_pine_blank_content_types_and_paragraphs_node_info() {
  $items = array(
    'about_pages' => array(
      'name' => t('About Pages'),
      'base' => 'node_content',
      'description' => t('Pages for the "About" section'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'article' => array(
      'name' => t('Article'),
      'base' => 'node_content',
      'description' => t('Use <em>articles</em> for time-sensitive content like news, press releases or blog posts.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'featured_highlight' => array(
      'name' => t('Featured Highlight'),
      'base' => 'node_content',
      'description' => t('Highlighted Feature which will appear in the Forestry Features section.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'file_upload' => array(
      'name' => t('File Upload'),
      'base' => 'node_content',
      'description' => t('Node that will provide a permanent link to an uploaded file.  Particularly useful for advising guides, student handbooks, meeting agendas, etc.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'highlighted_events' => array(
      'name' => t('Highlighted Events'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Event Title'),
      'help' => '',
    ),
    'node_block' => array(
      'name' => t('Node Block'),
      'base' => 'node_content',
      'description' => t('Pages that can be used as a block in theme regions or added as a block withing a page via paragraphs.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Basic page'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'paragraphs_page' => array(
      'name' => t('Basic Paragraphs Page'),
      'base' => 'node_content',
      'description' => t('Pages for the Undergraduate Program pages and associated content.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'student_quote' => array(
      'name' => t('Student Quote'),
      'base' => 'node_content',
      'description' => t('Student quotes and imagery for the Homepage slick carousel'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function cof_pine_blank_content_types_and_paragraphs_paragraphs_info() {
  $items = array(
    'anchor' => array(
      'name' => 'Anchor',
      'bundle' => 'anchor',
      'locked' => '1',
    ),
    'banner_image' => array(
      'name' => 'Banner Image',
      'bundle' => 'banner_image',
      'locked' => '1',
    ),
    'block_paragraph' => array(
      'name' => 'Block Paragraph',
      'bundle' => 'block_paragraph',
      'locked' => '1',
    ),
    'column_general_four' => array(
      'name' => '4 Column General - Responsive (four_column_general)',
      'bundle' => 'column_general_four',
      'locked' => '1',
    ),
    'feature_nav_block' => array(
      'name' => 'Feature Nav Block',
      'bundle' => 'feature_nav_block',
      'locked' => '1',
    ),
    'feature_nav_block_item' => array(
      'name' => 'Feature Nav Block Item',
      'bundle' => 'feature_nav_block_item',
      'locked' => '1',
    ),
    'headline' => array(
      'name' => 'Headline',
      'bundle' => 'headline',
      'locked' => '1',
    ),
    'highlighted_events' => array(
      'name' => 'Highlighted Events',
      'bundle' => 'highlighted_events',
      'locked' => '1',
    ),
    'icon_bar' => array(
      'name' => 'Icon Bar',
      'bundle' => 'icon_bar',
      'locked' => '1',
    ),
    'linked_list' => array(
      'name' => 'Linked List',
      'bundle' => 'linked_list',
      'locked' => '1',
    ),
    'mission_vision_statement' => array(
      'name' => 'Mission/Vision Statement',
      'bundle' => 'mission_vision_statement',
      'locked' => '1',
    ),
    'nav_block' => array(
      'name' => 'Nav Block',
      'bundle' => 'nav_block',
      'locked' => '1',
    ),
    'nav_block_item' => array(
      'name' => 'Nav Block Item',
      'bundle' => 'nav_block_item',
      'locked' => '1',
    ),
    'navigation_block_list' => array(
      'name' => 'Navigation Block List',
      'bundle' => 'navigation_block_list',
      'locked' => '1',
    ),
    'navigation_blocks' => array(
      'name' => 'Navigation Blocks',
      'bundle' => 'navigation_blocks',
      'locked' => '1',
    ),
    'news' => array(
      'name' => 'News + Events',
      'bundle' => 'news',
      'locked' => '1',
    ),
    'news_item' => array(
      'name' => 'News Item',
      'bundle' => 'news_item',
      'locked' => '1',
    ),
    'node_block_paragraph' => array(
      'name' => 'Node Block Paragraph',
      'bundle' => 'node_block_paragraph',
      'locked' => '1',
    ),
    'paragraph_basic' => array(
      'name' => 'Paragraph (Basic)',
      'bundle' => 'paragraph_basic',
      'locked' => '1',
    ),
    'paragraph_three_quarter' => array(
      'name' => 'Paragraph (3/4)',
      'bundle' => 'paragraph_three_quarter',
      'locked' => '1',
    ),
    'paragraph_with_image' => array(
      'name' => 'Paragraph with Image',
      'bundle' => 'paragraph_with_image',
      'locked' => '1',
    ),
    'paragraph_with_left_image' => array(
      'name' => 'Paragraph with Left Image',
      'bundle' => 'paragraph_with_left_image',
      'locked' => '1',
    ),
    'paragraph_with_right_image' => array(
      'name' => 'Paragraph with Right Image',
      'bundle' => 'paragraph_with_right_image',
      'locked' => '1',
    ),
    'possible_careers' => array(
      'name' => 'Possible Careers',
      'bundle' => 'possible_careers',
      'locked' => '1',
    ),
    'program_list' => array(
      'name' => 'Program List',
      'bundle' => 'program_list',
      'locked' => '1',
    ),
    'ribbon_slider' => array(
      'name' => 'Ribbon Slider',
      'bundle' => 'ribbon_slider',
      'locked' => '1',
    ),
    'section' => array(
      'name' => 'Section',
      'bundle' => 'section',
      'locked' => '1',
    ),
    'slide' => array(
      'name' => 'Slide',
      'bundle' => 'slide',
      'locked' => '1',
    ),
    'slide_article' => array(
      'name' => 'Slide Article',
      'bundle' => 'slide_article',
      'locked' => '1',
    ),
    'slide_container' => array(
      'name' => 'Slide Container',
      'bundle' => 'slide_container',
      'locked' => '1',
    ),
    'tab_block' => array(
      'name' => 'Tab Block',
      'bundle' => 'tab_block',
      'locked' => '1',
    ),
    'tabbed_section' => array(
      'name' => 'Tabbed Section',
      'bundle' => 'tabbed_section',
      'locked' => '1',
    ),
    'teaser' => array(
      'name' => 'Teaser',
      'bundle' => 'teaser',
      'locked' => '1',
    ),
    'testimonial' => array(
      'name' => 'Testimonial',
      'bundle' => 'testimonial',
      'locked' => '1',
    ),
    'testimonial_slider' => array(
      'name' => 'Testimonial Slider',
      'bundle' => 'testimonial_slider',
      'locked' => '1',
    ),
    'three_column_general' => array(
      'name' => '3 Column General - Responsive',
      'bundle' => 'three_column_general',
      'locked' => '1',
    ),
    'video_centered' => array(
      'name' => 'Video (Centered)',
      'bundle' => 'video_centered',
      'locked' => '1',
    ),
    'video_full_width' => array(
      'name' => 'Video (Full Width)',
      'bundle' => 'video_full_width',
      'locked' => '1',
    ),
    'web_biography' => array(
      'name' => 'Web Biography',
      'bundle' => 'web_biography',
      'locked' => '1',
    ),
    'web_biography_list' => array(
      'name' => 'Web Biography List',
      'bundle' => 'web_biography_list',
      'locked' => '1',
    ),
  );
  return $items;
}
